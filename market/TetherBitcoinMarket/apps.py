from django.apps import AppConfig


class TetherbitcoinmarketConfig(AppConfig):
    name = 'TetherBitcoinMarket'
