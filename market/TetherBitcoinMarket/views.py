# pylint: disable=maybe-no-member

from django.shortcuts import render
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets
from .models import Market, OrderBook, Bid, Ask, Trade
from .serializers import MarketSerializer, OrderBookSerializer, BidSerializer, AskSerializer
from rest_framework import status
import pytz
from decimal import Decimal
import time
from datetime import datetime, timedelta
import urllib
import json
import requests

# Create your views here.

def index(request):
    return render(request, 'TetherBitcoinMarket/index.html')

def initializeDB(n=50, ob=0):
    with urllib.request.urlopen('https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_BTC') as response:
        orderBook = json.load(response)

    asks = orderBook["asks"][:n]
    bids = orderBook["bids"][:n]

    for i in asks:
        price = float(i[0])
        amount = i[1]

        data = {
            "price":round(price, 2), 
            "coinAmount":round(amount, 10), 
            "orderBook":ob,
            "type": 2,
        }

        r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data) 


    for i in bids:
        price = float(i[0])
        amount = i[1]

        data = {
            "price":round(price, 2), 
            "coinAmount":round(amount, 10), 
            "orderBook":ob,
            "type": 3,
        }

        r = requests.post(url = "http://127.0.0.1:8000/api/USDT_BTC/orders/", json = data)

class ResetDB(APIView):
    def get_market(self, pk):
        try:
            return Market.objects.get(pk=pk)
        except Market.DoesNotExist:
            raise Http404
    
    def get_orderBook(self, pk):
        try:
            return OrderBook.objects.get(pk=pk)
        except OrderBook.DoesNotExist:
            raise Http404

    def get(self, request):
        requestType = request.GET.get("type", 0)
        if str(requestType) == "0":
            pk = request.GET.get("mk-pk", 0)
            Trade.objects.filter(market=self.get_market(pk=int(pk))).delete()
            Ask.objects.filter(orderBook=self.get_orderBook(pk=int(pk))).delete()
            Bid.objects.filter(orderBook=self.get_orderBook(pk=int(pk))).delete()
            OrderBook.objects.filter(pk=int(pk)).delete()
            Market.objects.filter(pk=int(pk)).delete()
            m = Market(id=int(pk))
            m.save()
            OrderBook(id=int(pk), market=m).save()
            initializeDB(ob=int(pk))
            currentTime = time.time()
            oneHourAgo = currentTime-60*60
            latestTradeUrl = 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=USDT_BTC&start='+str(oneHourAgo)+'&end='+str(currentTime)
            try: 
                latestTradeRequest = requests.get(latestTradeUrl)
                latestTrade = latestTradeRequest.json()[0]
                dt = Trade(price=latestTrade["rate"], coinAmount=latestTrade["amount"], market=m)
            except:
                dt = Trade(price=4000.0, coinAmount=1, market=m) 
            dt.save()
            return Response({"Response": "THE REQUEST IS COMPLETE"}, status=status.HTTP_202_ACCEPTED)
        elif str(requestType) == "1":
            envAmt = request.GET.get("env-amt", 0)
            Trade.objects.all().delete()
            Ask.objects.all().delete()
            Bid.objects.all().delete()
            Market.objects.all().delete()
            for i in range(int(envAmt)):
                m = Market(id=i)
                m.save()
                OrderBook(id=i, market=m).save()
                initializeDB(ob=i)
                currentTime = time.time()
                oneHourAgo = currentTime-60*60
                latestTradeUrl = 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=USDT_BTC&start='+str(oneHourAgo)+'&end='+str(currentTime)
                try: 
                    latestTradeRequest = requests.get(latestTradeUrl)
                    latestTrade = latestTradeRequest.json()[0]
                    dt = Trade(price=latestTrade["rate"], coinAmount=latestTrade["amount"], market=m)
                except:
                    dt = Trade(price=4000.0, coinAmount=1, market=m) 
                dt.save()
            return Response({"Response": "THE REQUEST IS COMPLETE"}, status=status.HTTP_202_ACCEPTED)
        elif str(requestType) == "2":
            Market.objects.all().delete()
            return Response({"Response": "THE REQUEST IS COMPLETE"}, status=status.HTTP_202_ACCEPTED)
        return Response({"Response": "ERROR-INVALID TYPE"}, status=status.HTTP_400_BAD_REQUEST)

class MarketTickerView(APIView):
    def get_object(self, pk):
        try:
            return Market.objects.get(pk=pk)
        except Market.DoesNotExist:
            raise Http404

    def get(self, request):
        try:
            marketPk = request.GET.get("pk", 0)
            market = self.get_object(marketPk)
            if len(Trade.objects.all()) > 0:
                last = Trade.objects.all().order_by('-timestamp')[0].price
            else:
                last = 0
            lowestAsk = market.orderBooks.all()[0].asks.filter().order_by('price')[0].price
            highestBid = market.orderBooks.all()[0].bids.filter().order_by('-price')[0].price
            time_threshold = datetime.now() - timedelta(hours=10000)
            oldSales = Trade.objects.filter(market=marketPk, timestamp__gt=time_threshold)
            if len(oldSales) > 0:
                oldPrice = oldSales.order_by('-timestamp')[0].price
            else:
                oldPrice = 0
            percentChange = (last-oldPrice)/last
            if market.currentDate.day != datetime.utcnow().day:
                market.currentDate=datetime.utcnow()
                market.baseVolume = market.currentBaseVolume
                market.quoteVolume = market.currentQuoteVolume
                market.currentBaseVolume = 0.0
                market.currentQuoteVolume = 0.0
                market.save()
            baseVolume = market.baseVolume
            quoteVolume = market.quoteVolume
            currentSales = datetime.now() - timedelta(hours=24)
            oldSales = Trade.objects.filter(market=marketPk, timestamp__gt=currentSales)
            if len(oldSales) > 0:
                high24hr = oldSales.order_by('-price')[0].price
            else:
                high24hr = 0
            if len(oldSales) > 0:
                low24hr = oldSales.order_by('price')[0].price
            else:
                low24hr = 0
            return Response({"last":last,"lowestAsk":lowestAsk,"highestBid":highestBid,"percentChange":percentChange,"baseVolume":baseVolume,"quoteVolume":quoteVolume,"isFrozen":0,"high24hr":high24hr,"low24hr":low24hr})
        except KeyError:
            return Response("Market ID not specified", status=status.HTTP_400_BAD_REQUEST)

class MarketView(APIView):

    def get_object(self, pk):
        try:
            return Market.objects.get(pk=pk)
        except Market.DoesNotExist:
            raise Http404

    def get(self, request):
        markets = Market.objects.all()
        serializer = MarketSerializer(markets, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        pk = request.GET.get("pk", 0)
        m = Market(id=int(pk))
        m.save()
        OrderBook(id=int(pk), market=m).save()
        initializeDB(ob=int(pk))
        currentTime = time.time()
        oneHourAgo = currentTime-60*60
        latestTradeUrl = 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=USDT_BTC&start='+str(oneHourAgo)+'&end='+str(currentTime)
        try: 
            latestTradeRequest = requests.get(latestTradeUrl)
            latestTrade = latestTradeRequest.json()[0]
            dt = Trade(price=latestTrade["rate"], coinAmount=latestTrade["amount"], market=m)
        except:
            dt = Trade(price=4000.0, coinAmount=1, market=m) 
        dt.save()
        return Response({"Response": "THE REQUEST IS COMPLETE"}, status=status.HTTP_201_CREATED)

    def delete(self, request):
        pk = request.GET.get("pk", 0)
        market = self.get_object(int(pk))
        market.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class OrderBookView(APIView):

    def get_object(self, pk):
        try:
            return OrderBook.objects.get(pk=pk)
        except OrderBook.DoesNotExist:
            raise Http404

    def get(self, request):
        orderBooks = OrderBook.objects.all()
        serializer = OrderBookSerializer(orderBooks, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = OrderBookSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"response": "The OrderBook is generated successfully."}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        orderBook = self.get_object(request.data["pk"])
        orderBook.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class OrderView(APIView):

    def getMarket(self, pk):
        try:
            return Market.objects.get(pk=pk)
        except Market.DoesNotExist:
            raise Http404

    def getOrderBook(self, pk):
        try:
            return OrderBook.objects.get(pk=pk)
        except OrderBook.DoesNotExist:
            raise Http404
            
    def getBid(self, orderBook, price, amount):
        # Get possible bids when conducting ask
        price = Decimal(price)
        amount = Decimal(amount)
        orders = orderBook.bids.filter(price__gte=price).order_by('-price')
        for i in orders:
            if i.coinAmount >= amount:
                i.coinAmount -= amount
                cueDelete = False
                if i.coinAmount - amount == 0:
                    cueDelete = True
                else:
                    i.save()
                trade = Trade()
                trade.price = i.price
                trade.coinAmount = amount
                trade_market = self.getMarket(int(orderBook.market.id))
                trade.market = trade_market
                trade_market.currentBaseVolume+=i.price*amount
                trade_market.currentQuoteVolume+=amount
                trade_market.save()
                trade.save()
                if cueDelete:
                    i.delete()
                return 0
            else:
                # difference = amount - i.coinAmount
                trade = Trade()
                trade.price = i.price
                trade.coinAmount = i.coinAmount
                trade_market = self.getMarket(int(orderBook.market.id))
                trade.market = trade_market
                trade_market.currentBaseVolume+=i.price*amount
                trade_market.currentQuoteVolume+=amount
                trade_market.save()
                trade.save()
                i.delete()
                return [price, amount]
        return None

    def getAsk(self, orderBook, price, amount):
        # Get possible asks when conducting bid
        price = Decimal(price)
        amount = Decimal(amount)
        orders = orderBook.asks.filter(price__lte=price).order_by('price')
        for i in orders:
            if i.coinAmount >= amount:
                i.coinAmount -= amount
                cueDelete = False
                if i.coinAmount <= 0:
                    cueDelete = True
                else:
                    i.save()
                trade = Trade()
                trade.price = i.price
                trade.coinAmount = amount
                trade_market = self.getMarket(int(orderBook.market.id))
                trade.market = trade_market
                trade_market.currentBaseVolume+=i.price*amount
                trade_market.currentQuoteVolume+=amount
                trade_market.save()
                trade.save()
                if cueDelete:
                    i.delete()
                return 0
            else:
                # difference = amount - i.coinAmount
                trade = Trade()
                trade.price = i.price
                trade.coinAmount = i.coinAmount
                trade_market = self.getMarket(int(orderBook.market.id))
                trade.market = trade_market
                trade_market.currentBaseVolume+=i.price*amount
                trade_market.currentQuoteVolume+=amount
                trade_market.save()
                trade.save()
                i.delete()
                return [price, amount]
        return None

    def get(self, request):
        bids = Bid.objects.all()
        asks = Ask.objects.all()
        bidsSerializer = BidSerializer(bids, many=True)
        asksSerializer = AskSerializer(asks, many=True)
        return Response({"bids": bidsSerializer.data,"asks": asksSerializer.data})

    def post(self, request):
        print(request.data)
        if request.data["type"] == 1:
            del request.data["type"]
            orderBook = self.getOrderBook(request.data["orderBook"])
            tradeResult = self.getBid(orderBook, request.data["price"], request.data["coinAmount"])
            if not (tradeResult):
                serializer = AskSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.save()
                    print("market_log: C1 O")
                    return Response({"response": "The order is booked successfully."}, status=status.HTTP_201_CREATED)
            if type(tradeResult) == list:
                request.data["coinAmount"] = tradeResult[1]
                serializer = AskSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.save()
                    print("market_log: C1 OT")
                    return Response({"response": "The order, along with a tansaction, is booked successfully."}, status=status.HTTP_201_CREATED)
            if tradeResult == 0:
                print("market_log: C1 T")
                return Response({"response": "A transaction is placed successfully."}, status=status.HTTP_200_OK)
            return Response({"response": "Bad Request."}, status=status.HTTP_400_BAD_REQUEST)
        elif request.data["type"] == 0:
            del request.data["type"]
            orderBook = self.getOrderBook(request.data["orderBook"])
            tradeResult = self.getAsk(orderBook, request.data["price"], request.data["coinAmount"])
            
            if not (tradeResult):
                serializer = BidSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.save()
                    print("market_log: C0 O")
                    return Response({"response": "The order is booked successfully."}, status=status.HTTP_201_CREATED)
            if type(tradeResult) == list:
                request.data["coinAmount"] = tradeResult[1]
                serializer = BidSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.save()
                    print("market_log: C0 OT")
                    return Response({"response": "The order, along with a tansaction, is booked successfully."}, status=status.HTTP_201_CREATED)
            if tradeResult == 0:
                print("market_log: C0 T")
                return Response({"response": "A transaction is placed successfully."}, status=status.HTTP_200_OK)
            return Response({"response": "Bad Request."}, status=status.HTTP_400_BAD_REQUEST)
        elif request.data["type"] == 2:
            serializer = AskSerializer(data=request.data)
            if serializer.is_valid():
                    serializer.save()
                    return Response({"response": "The system has recieved the order."}, status=status.HTTP_201_CREATED)
            else:
                print(serializer.errors)
                return Response({"response": "The system has raised an error."}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        elif request.data["type"] == 3:
            serializer = BidSerializer(data=request.data)
            if serializer.is_valid():
                    serializer.save()
                    return Response({"response": "The system has recieved the order."}, status=status.HTTP_201_CREATED)
            else:
                print(serializer.errors)
                return Response({"response": "The system has raised an error."}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response({"response": "Bad Request."}, status=status.HTTP_400_BAD_REQUEST)
