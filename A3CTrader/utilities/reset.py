import requests

def single_market_reset(marketPk):
    """
    Resets and conforms a single market

    Inputs:
        int: market PK

    Returns none
    """
    requests.get(url = "http://127.0.0.1:8000/api/USDT_BTC/reset/?type=0&mk-pk="+str(marketPk)) 

def database_reset(newMarketCount):
    """
    Clears the database and makes n new markets

    Input:
        int: new market count

    Returns none
    """
    requests.get(url = "http://127.0.0.1:8000/api/USDT_BTC/reset/?type=1&env-amt="+str(newMarketCount)) 

def database_wipe():
    """
    Nukes the database

    Inputs none

    Returns none
    """
    requests.get(url = "http://127.0.0.1:8000/api/USDT_BTC/reset/?type=2") 

