# (c) 2019 Houjun Liu
# le little Trader | Menlo Park, CA
# Sets up enviroment class
# pylint: disable=maybe-no-member
# pylint: disable=import-error

import time
import random
from utilities import markets, reset

class Enviroment(object):
    """
    An Market Enviroment object used to interact with RL models
    """

    def __init__(self, target=0.1, **kwargs):
        if "wait" in kwargs: 
            self.waitSec = kwargs["wait"]
        else:
            self.waitSec = None
        if "pk" in kwargs:
            self.pk = kwargs["pk"]
        else:
            self.pk = markets.get_smallest_unused()
            markets.new(self.pk)
        if "del" in kwargs:
            self.delete = True
        else:
            self.delete = False
        self.targetPrice = float(markets.get_ticker(self.pk)[0])*target

    def getRawInputData(self):
        """
        Gets the current slice of enviroment data from market

        Inputs none
        Outputs:
            1dx206 array: *kw data
        """
        flatten = lambda l: [item for sublist in l for item in sublist]
        result = (markets.get_ticker(self.pk) + flatten(markets.get_order_book(self.pk, 0) + markets.get_order_book(self.pk, 1)))
        return result


class BearMarket(Enviroment):
    def __init__(self, target=0.1, **kwargs):
        if "wait" in kwargs: 
            waitSec = kwargs["wait"]
        else:
            waitSec = None
        if "pk" in kwargs:
            pk = kwargs["pk"]
        else:
            pk = markets.get_smallest_unused()
            markets.new(self.pk)
        if "del" in kwargs:
            delete = True
        else:
            delete = False
        super().__init__(target, wait=waitSec, pk=pk, delete=delete)

    def getNextStateReward(self, action, r=False):
        """
        Gets a new state and reward for taking an action

        Inputs:
            1dx4 array: [float_price, float_amount, int_buy, int_sell]

        Outputs:
            tuple: (array1dx6_inputSlice, float_reward)
        """
        if -1e-3 < action[0] < 1e-3:
            return self.getRawInputData(), -5
        oldPrice = markets.get_ticker(self.pk)[0]
        if r:
           action = action[:2].tolist()+random.sample([[0, 1], [1, 0]], 1)[0]
        markets.post_order(action, self.pk)
        if self.waitSec:
            time.sleep(self.waitSec)
        currentPrice = markets.get_ticker(self.pk)[0]
        decrease = oldPrice-currentPrice
        distanceFromTarget = currentPrice-self.targetPrice
        normalizer = lambda x, y: ((y**float(x))-(y**(-float(x))))/((y**float(x))+(y**(-float(x))))
        regularReward = decrease+normalizer(distanceFromTarget, 1.0001)
        if -1<regularReward<1:
            regularReward = -2
        return self.getRawInputData(), regularReward

class BullMarket(Enviroment):
    def __init__(self, target=1.1, **kwargs):
        if "wait" in kwargs: 
            waitSec = kwargs["wait"]
        else:
            waitSec = None
        if "pk" in kwargs:
            pk = kwargs["pk"]
        else:
            pk = markets.get_smallest_unused()
            markets.new(self.pk)
        if "del" in kwargs:
            delete = True
        else:
            delete = False
        super().__init__(target, wait=waitSec, pk=pk, delete=delete)

    def getNextStateReward(self, action, r=False):
        if -1e-3 < action[0] < 1e-3:
            return self.getRawInputData(), -5
        """
        Gets a new state and reward for taking an action

        Inputs:
            1dx4 array: [float_price, float_amount, int_buy, int_sell]

        Outputs:
            tuple: (array1dx6_inputSlice, float_reward)
        """
        oldPrice = markets.get_ticker(self.pk)[0]
        if r:
           action = action[:2].tolist()+random.sample([[0, 1], [1, 0]], 1)[0]
        markets.post_order(action, self.pk)
        if self.waitSec:
            time.sleep(self.waitSec)
        currentPrice = markets.get_ticker(self.pk)[0]
        increase = currentPrice-oldPrice
        distanceFromTarget = self.targetPrice-currentPrice
        normalizer = lambda x, y: ((y**float(x))-(y**(-float(x))))/((y**float(x))+(y**(-float(x))))
        regularReward = increase+normalizer(distanceFromTarget, 1.0001)
        if -1<regularReward<1:
            regularReward = -2
        return self.getRawInputData(), regularReward

