# (c) 2019 Houjun Liu
# le little Trader | Menlo Park, CA
# Sets up unithread and multithread agents
# Creates algorithum for a2c
# pylint: disable=maybe-no-member
# pylint: disable=import-error
# pylint: disable=not-context-manager

import time
import logging
import coloredlogs
import random
import data
import queue
import enviroment
import threading
import numpy as np
from utilities import markets, reset
import keras.backend as K
import tensorflow as tf
from keras.initializers import Constant
from keras.optimizers import Adam
from keras.models import Sequential, Model
from keras.layers import Input, Dense, BatchNormalization, Concatenate, Dropout

coloredlogs.install()

logging.getLogger("urllib3").setLevel(logging.WARNING)

class Agent(object):
    """
    The Worker Agent Class
    """

    def __init__(self, enviroment, **kwargs):
        """
        Sets up graph
        """

        self.graph = tf.Graph()
        self.sess = tf.Session(graph=self.graph)

        self.memory = []
        self.replay = []
        self.count = -1
        self.env = enviroment
        if "gamma" in kwargs:
            self.gamma = kwargs["gamma"]
        else:
            self.gamma = 0
        if "name" in kwargs:
            self.name = kwargs["name"]
        else:
            self.name = "untitled"
        if "master" in kwargs:
            self.isMaster = kwargs["master"]
        else:
            self.isMaster = False

        # K.set_session(self.sess)
        with self.sess.as_default():
            with self.graph.as_default():

                marketDataInput = Input(shape=(206,), name="MarketDataInput")
                orderInput = Input(shape=(4,), name="ActorOut_In")

                a = BatchNormalization()(marketDataInput)
                b = Dense(5)(a)
                c = Dense(15, kernel_initializer=Constant(value=1e-5))(a)
                d = Dropout(0.7)(c)
                priceLayer = Dense(1, activation='relu', name="PriceOut")(b)
                amountLayer = Dense(1, activation='relu', name="AmountOut")(d)
                orderLayer = Dense(2, activation='sigmoid', name="OrderTypeOut")(c)
                actorOut = Concatenate(name="ActorOut")([priceLayer, amountLayer, orderLayer])

                criticInput = Concatenate()([orderInput, a])
                e = Dropout(0.8)(criticInput)
                # , kernel_initializer=Constant(value=1e-10)
                f = Dense(3, activation='tanh')(e)
                criticOut = Dense(1, name="CriticOut")(f)

                visualizerInput = Concatenate()([actorOut, a])
                g = Dropout(0.8)(visualizerInput)
                # , kernel_initializer=Constant(value=1e-10)
                h = Dense(3, activation='tanh')(g)
                visualizerOut = Dense(1, name="CriticOut")(h)

                self.__subActorModel = Model(inputs=marketDataInput, outputs=actorOut)
                self.__subActorModel.compile(
                    loss="mse",
                    optimizer=Adam(lr=7e-4, decay=2e-4),
                    metrics=['accuracy']
                )

                # Not private
                self._visualizer = Model(inputs=marketDataInput, outputs=visualizerOut)
                self._visualizer.compile(
                    loss="mse",
                    optimizer=Adam(lr=7e-4, decay=2e-4),
                    metrics=['accuracy']
                )

                self.actorModel = Model(inputs=marketDataInput, outputs=actorOut)
                self.actorModel.compile(
                    loss="categorical_crossentropy",
                    optimizer=Adam(lr=2e-5),
                    metrics=['accuracy']
                )

                self.criticModel = Model(inputs=[marketDataInput, orderInput], outputs=criticOut)
                self.criticModel.compile(
                    loss="mse",
                    optimizer=Adam(lr=1e-2),
                    metrics=['accuracy']
                )

                self.criticModel._make_predict_function()
                self.criticModel._make_test_function()
                self.criticModel._make_train_function()

                self.actorModel._make_predict_function()
                self.actorModel._make_test_function()
                self.actorModel._make_train_function()

    def __sinrelu(self, x):
        """
        ~Private~
        [SinRelu](https://medium.com/@wilder.rodrigues/sinerelu-an-alternative-to-the-relu-activation-function-e46a6199997d)
        
        Inputs:
            tensor: layer to be activated

        Outputs:
            tensor: layer that's activated
        """

        if K.greater(x, K.constant(0)) is not None:
            return x
        else:
            1e-10*(K.sin(x)-K.cos(x))

    def preTrain(self, ds=1000, valSplit=0.1, batchSize=10, epoch=10, **kwargs):
        """
        ~PRIVATE~
        Trains actor for a baseline value

        Inputs:
            optional int: ds dataSize
            optional float: valSplit validation percentage (between 0-1)
            optional int: batch size
            optional int: epoch
            optional bool: shuffle data between epochs
        Returns none
        """

        if "shuffle" in kwargs:
            shuffle = kwargs["shuffle"]
        else:
            shuffle = False
        
        inputData, outputData = data.load_agent_train_data(ds)

        # K.set_session(self.sess)
        with self.sess.as_default():
            with self.graph.as_default():
                self.__subActorModel.fit(x=[inputData], y=[outputData], validation_split=valSplit, batch_size=batchSize, epochs=epoch, shuffle=shuffle)
    
    def acStep(self, s):
        """
        Experience and train a single episode

        Inputs:
            1dx206 array: scene to be interacted upon
        ReturnsL
            1dx206 array: next scene
            1dx2 tuple: gradients for this propergation, critic, actor
        """

        # K.set_session(self.sess)
        with self.sess.as_default():
            with self.graph.as_default():
                action = self.actorModel.predict([[s]])
                v = self.criticModel.predict([np.array([s]), np.array([action[0]])])
                nextState, reward = self.env.getNextStateReward(action[0], True)
                vPrime  = self.criticModel.predict([np.array([nextState]), np.array([self.actorModel.predict([[nextState]])[0]])])

                value = reward+(self.gamma*vPrime)

                logging.debug(self.name+" assigned "+str(v)+" for "+str(value))

                criticIn = [np.array([s]), np.array([action[0]])]
                actorIn = [[s]]
                criticOut = [value]
                actorOut = [[value[0].tolist()*4]]
                fita = self.criticModel.fit(x=criticIn, y=criticOut, verbose=0)
                fitb = self.actorModel.fit(x=actorIn, y=actorOut, verbose=0)

                logging.info(self.name + "   Critic loss:" + str(fita.history["loss"]) + "    Actor loss: " + str(fitb.history["loss"]))


                criticGradOp = K.gradients(self.criticModel.output, self.criticModel.trainable_weights)
                actorGradOp = K.gradients(self.actorModel.output, self.actorModel.trainable_weights)

                criticGrads, actorGrads = self.sess.run([criticGradOp, actorGradOp], feed_dict={self.criticModel.input[0]: criticIn[0], self.criticModel.input[1]: criticIn[1], self.actorModel.input: actorIn[0]})

                return nextState, (criticGrads, actorGrads)

    def set_weights(self, criticWeights, actorWeights):
        """
        Sets the weights of the model

        Keras Weights: critic weights (symbolic)
        Keras Weights: actor weights (symbolic)
        """

        # K.set_session(self.sess)
        with self.sess.as_default():
            with self.graph.as_default():
                self.criticModel.set_weights(criticWeights)
                self.actorModel.set_weights(actorWeights)

    def get_weights(self):

        # K.set_session(self.sess)
        with self.sess.as_default():
            with self.graph.as_default():
                return self.criticModel.get_weights(), self.actorModel.get_weights()

    def apply_grads(self, criticGrads, actorGrads):
        """
        Applies gradients to both actor and critic

        Inputs:
            numpy array: critic grads (numerical)
            numpy array: actor grads (numerical)

        Returns none
        """

        # K.set_session(self.sess)
        with self.sess.as_default():
            with self.graph.as_default():
                for i in range(len(self.criticModel.trainable_weights)):
                    layer = self.criticModel.trainable_weights[i]
                    # criticLR = K.eval(self.criticModel.optimizer.lr)
                    criticLR = self.sess.run(self.criticModel.optimizer.lr)
                    criticUpdateOp = tf.assign_sub(layer, criticLR * criticGrads[i])
                    self.sess.run(criticUpdateOp)
                for i in range(len(self.actorModel.trainable_weights)):
                    layer = self.actorModel.trainable_weights[i]
                    # actorLR = K.eval(self.actorModel.optimizer.lr)
                    actorLR = self.sess.run(self.actorModel.optimizer.lr)
                    actorUpdateOp = tf.assign_sub(layer, actorLR * actorGrads[i])
                    self.sess.run(actorUpdateOp)

class Controller(object):
    def __init__(self, master, num_of_workers, wait=0, verbose=1):
        assert type(master) == Agent, "Master to be added is not of class Agent!"
        self.agentLibrary = []
        self.__lockLibrary = []
        self.__workerLibrary = []
        self.master = master
        self.workerCount = num_of_workers
        self.__taskQueue = queue.Queue()
        self.__exit = False
        self.__compiled = False
        self.wait = wait

        if verbose == 0:
            coloredlogs.set_level(logging.ERROR)
        elif verbose == 1:
            coloredlogs.set_level(logging.INFO)
        elif verbose == 2:
            coloredlogs.set_level(logging.DEBUG)
        elif verbose == -1:
            coloredlogs.set_level(logging.FATAL)
    
    def add(self, *args):
        assert not self.__compiled, "Controller compiled! Cannot add new agents!"
        if type(args[0]) == list and len(args) == 1:
            agents = args[0]
        else:
            agents = args
        for a in agents:
            assert type(a) == Agent, str(a)+" is not of type Agent"
            self.agentLibrary.append(a)

    def compile(self):
        self.__lockLibrary.append(threading.RLock())
        for _ in range(self.workerCount):
            self.__workerLibrary.append(threading.Thread(target=self.__work))
        for i, _ in enumerate(self.agentLibrary):
            self.__taskQueue.put({"agent": i, "type": "build", "content": None}, block=False)
            self.__lockLibrary.append(threading.RLock())
        self.__compiled = True

    def run(self, ttl):
        for thread in self.__workerLibrary:
            thread.start()
            time.sleep(self.wait)
        start = time.time()
        while time.time() <= start+ttl:
            pass
        self.__exit = True

    # def __buildFrom(self, i):
    #     logging.info("Good day! I am builder thread " + str(threading.get_ident()))
    #     for i in range(i, len(self.agentLibrary)):
    #         self.__taskQueue.put({"agent": i, "type": "build", "content": None})
    #         self.__lockLibrary.append(threading.RLock())
    #     logging.info("Good bye! Builder thread " + str(threading.get_ident())+" finished submitting all agents")

    def __work(self):
        logging.info("Good day! I am " + str(threading.get_ident()))
        # NOTE: MUST BE CALLED IN A NON-MASTER THREAD
        while self.__exit != True:  
            logging.debug(str(threading.get_ident())+" ready, getting task...")
            task = self.__taskQueue.get()
            taskType = task["type"]
            agent = self.agentLibrary[task["agent"]]
            lock = self.__lockLibrary[task["agent"]+1]
            masterLock = self.__lockLibrary[0]
            content = task["content"]
            logging.debug(str(threading.get_ident()) + " serving A" + str(task["agent"]) + " for " + str(taskType) + "\tQue Size:" + str(self.__taskQueue.qsize()) )
            # K.set_session(agent.sess)
            with agent.sess.as_default():
                if taskType == "build":
                    lock.acquire()
                    criticWeights, actorWeights = self.master.get_weights()
                    agent.set_weights(criticWeights, actorWeights)
                    agent.name = str(task["agent"])
                    self.__taskQueue.put({"agent": task["agent"], "type": "a3cStep", "content": agent.env.getRawInputData()})
                    lock.release()
                elif taskType == "updateWeights":
                    lock.acquire()
                    criticWeights, actorWeights = self.master.get_weights()
                    agent.set_weights(criticWeights, actorWeights)
                    lock.release()
                elif taskType == "a3cStep":
                    lock.acquire()
                    nextState, grads = agent.acStep(content)
                    masterLock.acquire()
                    criticGrads = grads[0]
                    actorGrads = grads[1]
                    self.master.apply_grads(criticGrads, actorGrads)
                    criticWeights, actorWeights = self.master.get_weights()
                    agent.set_weights(criticWeights, actorWeights)
                    masterLock.release()
                    self.__taskQueue.put({"agent": task["agent"], "type": "a3cStep", "content": nextState})
                    lock.release()
        logging.info("Good bye! I was " + str(threading.get_ident()))
