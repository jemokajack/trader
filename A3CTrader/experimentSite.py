# (c) 2019 Houjun Liu
# le little Trader | Menlo Park, CA
# Frontend
# pylint: disable=maybe-no-member
# pylint: disable=import-error
# pylint: disable=not-context-manager
# pylint: disable=import-error

import os
import uuid
import random
import pickle
import datetime
import agent, enviroment, utilities
from keras.utils.vis_utils import plot_model

def new():
    print(chr(27) + "[2J")
    missionID = random.randint(10000, 99999)
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 1]

Ok, here we go. Firstly, bear or bull?
    """)
    op = input("1|(bear=b,bull=u)|")
    if op == "b":
        agentType = 0
    elif op == "u":
        agentType = 1
    elif op == "bear":
        agentType = 0
    elif op == "bull":
        agentType = 1
    else:
        raise Exception("That's not one of the options you numpty!")
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 2]

Sure. How many traders then?
(Remember that one more will be created as master)
    """)
    op = input("2|(amount=[int])|")
    agentAmount = int(op)
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 3]

Ok. How many workers should serve these agents?
You mileage may vary as far as processing abilities.
(Remember that the amount of threads created will be close to, but not the number here)
    """)
    op = input("3|(amount=[int])|")
    threadCount = int(op)
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 4]

Let's talk about pre-training. Answer the following questions in order.

1. How much data should I load for PT?
2. What should the PT batch size be?
3. How many epochs should I do PT for?
    """)
    op = input("4.1|(amount=[int])|")
    preTrainDS = int(op)
    op = input("4.2|(count=[int])|")
    preTrainBS = int(op)
    op = input("4.3|(epochs=[int])|")
    preTrainEpoch = int(op)
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 5]

Ok, last thing. A few questions about training controller.

1. How chatty should the controller be? 
   (Verbosity 0 = Error, Verbosity 1 = Losses, Verbosity 2 = Everything, Verbosity -1 = Nothing)
2. How long should the A3C training take (minutes)?
    """)
    op = input("5.1|(verbosity=[int])|")
    verbosity = int(op)
    op = input("5.2|(ttl=[int])|")
    ttl = int(op)*60
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 6]

Thank you for the info. Here's a summary:

Mission M{} to create agents with the A3C Trader
---------------------------------------------------
Controller                                        
TTL                      |      {} Seconds          
Verbosity                |      Level {}  
Worker Count             |      {} Workers                
---------------------------------------------------
Agents
Market Type              |      {}
Count                    |      {} Agents
---------------------------------------------------
PreTrain
Data Size                |      {} Items
Batch Size               |      {} Items/Batch
Epoch                    |      {} Epochs
---------------------------------------------------

Please confirm the information presented above
    """.format(missionID, ttl, verbosity, threadCount, "Bear" if agentType==0 else "Bull", agentAmount, preTrainDS, preTrainBS, preTrainEpoch))
    input("6|CONFIRM>")
    print("\n Got it. Creating mission...")
    mission = ["M"+str(missionID), ttl, verbosity, threadCount, agentType, agentAmount, preTrainDS, preTrainBS, preTrainEpoch]
    saveID = str(uuid.uuid4())
    configs[saveID] = mission[1:]
    saveMissions()
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 7]

Thank you for the mission. Here are the confirmation codes.
Mission ID (for current session ONLY) = M{}
Record ID = {} <- WRITE THIS DOWN SOMEWHERE
Timestamp = {}

Ok, are we ready for fire? Feel free to Ctrl+C now to simply save record and not run.
    """.format(missionID, saveID, str(datetime.datetime.now())))
    input("7|FIRE?>")
    run(mission)

def saveMissions():
    with open("trader.missions", "wb") as file:
        pickle.dump(configs, file)

def getVisualization():
    print(chr(27) + "[2J")
    missionID = random.randint(10000, 99999)
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 1]

Let's do this. What's the UUID of the model config? (WITH dashes)
    """)
    op = str(input("1|(id=[UUID])|")).lower()
    record = op
    dataArray = configs[op]
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 2]

Thank you for that. Please provide a path to which the visualization will be saved.
    """)
    op = input("2|(path=path)|")
    path = str(op)
    mission = ["M"+str(missionID), dataArray[0], dataArray[1], dataArray[2], dataArray[3], dataArray[4], dataArray[5], dataArray[6], dataArray[7], path]
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 2]

Thank you for the info. Here's a summary:

Recovery Mission M{} to visualize agents with the A3C Trader
---------------------------------------------------
Controller                                        
TTL                      |      {} Seconds          
Verbosity                |      Level {}  
Worker Count             |      {} Workers                
---------------------------------------------------
Agents
Market Type              |      {}
Count                    |      {} Agents
---------------------------------------------------
PreTrain
Data Size                |      {} Items
Batch Size               |      {} Items/Batch
Epoch                    |      {} Epochs
---------------------------------------------------
Visualization saved to: {}

Please confirm the information presented above
    """.format(missionID, dataArray[0], dataArray[1], dataArray[2], "Bear" if dataArray[3]==0 else "Bull", dataArray[4], dataArray[5], dataArray[6], dataArray[7], path))
    input("2|CONFIRM>")
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 4]

Thank you for the mission. Here are the confirmation codes.
Mission ID (for current session ONLY) = M{}
Record ID = {}
Timestamp = {}

Ok, are we ready for fire?
    """.format(missionID, record, str(datetime.datetime.now())))
    input("4|FIRE?>")
    visualize(mission)

def visualize(mission):
    print(chr(27) + "[2J")
    print("HELLO, I am runner", str(uuid.uuid4()), "serving mission "+str(mission[0]))
    print("Creating tasks....")
    print("Step 1: Creating Market")
    if mission[4] == 0:
        env = enviroment.BearMarket(wait=10, pk=0)
    elif mission[4] == 1:
        env = enviroment.BullMarket(wait=10, pk=0)
    print("Step 2: Creating Agent")
    a = agent.Agent(env)
    criticPath = os.path.join(mission[-1], str(mission[0]).replace('"', '')+"_critic.png")
    actorPath = os.path.join(mission[-1], str(mission[0]).replace('"', '')+"_actor.png")
    visualizerPath = os.path.join(mission[-1], str(mission[0]).replace('"', '')+".png")
    plot_model(a.criticModel, to_file=criticPath, show_shapes=True)
    plot_model(a.actorModel, to_file=actorPath, show_shapes=True)
    plot_model(a._visualizer, to_file=visualizerPath, show_shapes=True)
    print("Step 3: Well... There's no step 3. Check the directory! It's there!")

def qs(mission, threads, ttl):
    print(chr(27) + "[2J")
    print("HELLO, I am runner", str(uuid.uuid4()), "serving mission "+str(mission))
    print("Be patient, this will take a while.")
    ptSize = [210000, 150000, 100000, 50000]
    ptEpoch = [2, 5, 10]
    count = [10, 20, 30, 40, 50, 60, 70, 80, 90]
    aTypes = [0, 1]
    bears = []
    bulls = []
    for i in range(2):
        for e in range(4):
            for f in range(3):
                for g in range(9):
                    agentID = str(uuid.uuid4())
                    print("\r Creating agent group {}-{}-{}-{}".format(i, e, f, g), end="")
                    mission = [ttl, 2, threads, aTypes[i], count[g], ptSize[e], 15, ptEpoch[f]]
                    configs[agentID] = mission
                    if aTypes[i] == 0:
                        bears.append(agentID)
                    elif aTypes[i] == 1:
                        bulls.append(agentID)
                    saveMissions()
    input("We are ready to report. Press enter to see all bear codes to populate the sheet...")
    for code in bears:
        print(code)
    input("Now let's do the bulls. Press enter to see all bull codes to populate the sheet...")
    for code in bulls:
        print(code)



def quickStart():
    print(chr(27) + "[2J")
    missionID = random.randint(10000, 99999)
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 1]

Let's do this. This could take a while.....
Before we start, two questions.
1. How many threads should each agent employ?
2. How long will the A3C process be (minutes)?
    """)
    op = input("1.1|(threads=[int])|")
    threads = int(op)
    op = input("1.2|(ttl=[int])|")
    ttl = int(op)*60
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 2]

Ok, just to confirm.
Mission {} to quickstart.
--------------------------
/agent uses {} threads and lives for {} minutes

Please confirm below
    """)
    input("2|FIRE?>")
    qs(missionID, threads, ttl/60)

def loadMission():
    print(chr(27) + "[2J")
    missionID = random.randint(10000, 99999)
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 1]

Let's do this. What's the UUID of the saved config? (WITH dashes)
    """)
    op = str(input("1|(id=[UUID])|")).lower()
    dataArray = configs[op]
    mission = ["M"+str(missionID), dataArray[0], dataArray[1], dataArray[2], dataArray[3], dataArray[4], dataArray[5], dataArray[6], dataArray[7]]
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 2]

Thank you for the info. Here's a summary:

Recovery Mission M{} to recover agents with the A3C Trader
---------------------------------------------------
Controller                                        
TTL                      |      {} Seconds          
Verbosity                |      Level {}  
Worker Count             |      {} Workers                
---------------------------------------------------
Agents
Market Type              |      {}
Count                    |      {} Agents
---------------------------------------------------
PreTrain
Data Size                |      {} Items
Batch Size               |      {} Items/Batch
Epoch                    |      {} Epochs
---------------------------------------------------

Please confirm the information presented above
    """.format(missionID, dataArray[0], dataArray[1], dataArray[2], "Bear" if dataArray[3]==0 else "Bull", dataArray[4], dataArray[5], dataArray[6], dataArray[7]))
    input("2|CONFIRM>")
    print("\n Got it. Recovering mission...")
    print(chr(27) + "[2J")
    print("""
ExperimentSite alpha ~0.0.1 04062019
------------------------------------
[WINDOW 3]

Thank you for the mission. Here are the confirmation codes.
Mission ID (for current session ONLY) = M{}
Record ID = {}
Timestamp = {}

Ok, are we ready for fire?
    """.format(missionID, op, str(datetime.datetime.now())))
    input("3|FIRE?>")
    run(mission)



def run(mission):
    print(chr(27) + "[2J")
    print("HELLO, I am runner", str(uuid.uuid4()), "serving mission "+str(mission[0]))
    print("Creating tasks....")
    print("Step 1: Creating Market")
    if mission[4] == 0:
        env = enviroment.BearMarket(wait=10, pk=0)
    elif mission[4] == 1:
        env = enviroment.BullMarket(wait=10, pk=0)
    print("Step 2: Creating Master")
    masterAgent = agent.Agent(env, master=True)
    print("Step 3: Creating Agents")
    agents = []
    for i in range(mission[5]):
        print("\rMainthread is creating Agent",i, end="")
        agents.append(agent.Agent(env))
    print()
    print("Step 4: Pretraining")
    masterAgent.preTrain(ds=mission[6], batchSize=mission[7], epoch=mission[8], shuffle=True)
    print("Step 5: Creating Controller")
    controller = agent.Controller(masterAgent, mission[3], wait=5, verbose=mission[2])
    controller.add(agents)
    controller.compile()
    print("Step 6: Running..... Handing sysout stream to train controller desk")
    controller.run(mission[1])
    print("Step 7: Exiting..... Thank you.")

try: 
    with open("trader.missions", "rb") as file:
        configs = pickle.load(file)
except FileNotFoundError:
    with open("trader.missions", "wb") as file:
        pickle.dump({}, file)
    configs = {}


print(chr(27) + "[2J")
print("""
Good day Mankind!
----------------------------------------------------------
This is the ExperimentSite frontend alpha ~0.0.1 04062019
----------------------------------------------------------
[WINDOW 0]

0. New
1. Load
2. Nuke Database
3. Get Visualizations
4. Quickstart for Experimentation
""")
op = input("0|(new=0,load=1,reset=2,visualize=3,qs=4)|")

if op == "0":
    new()
elif op == "1":
    loadMission()
elif op == "2":
    utilities.reset.database_wipe()
elif op == "3":
    getVisualization()
elif op == "4":
    quickStart()
elif op == "q":
    pass
else:
    raise Exception("That's not one of the options you numpty!")

print("That's it. End of execution. Dada más. Press enter to exit.")
input()
print(chr(27) + "[2J")
print("Beep Beep Boop. Have a great day!")
print("Signing off | ExperimentSite · alpha ~0.0.1 04062019")
