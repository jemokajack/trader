import requests
from decimal import Decimal
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import csv
import sys

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)
xar = []
yar = []
history = []
removect = 0
count = 0

def getPortfolioValue(coin, fund):
    tickerUrl = 'http://127.0.0.1:8000/api/USDT_BTC/ticker/?pk=0'
    try:
        tickerData = requests.get(tickerUrl).json()
        return Decimal(tickerData["last"])+Decimal(fund), 0
    except:
        return getPortfolioValue(coin, fund)[0], 1

def animate(i):
    global removect
    global count
    pV, sW = getPortfolioValue(1, 0)

    with open('marketDataTraining.csv', 'a', newline='') as dataFile:
        writer = csv.writer(dataFile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow([count, pV, datetime.now()])
        if sW == 1:
            count += 1
    
    # with open(sys.argv[1], 'a', newline='') as dataFile:
    #     writer = csv.writer(dataFile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    #     writer.writerow([count, pV, datetime.now()])
    #     count += 1

    # xar.append(datetime.now())
    # history.append(pV)

    # removect += 1
    # if removect >= 1000:
    #     yar.pop(0)
    #     xar.pop(0)
    #     removect = 0

    # ax1.clear()
    # ax1.plot_date(xar,yar,"o", markersize=2)

    xar.append(datetime.now())
    history.append(pV)
    yar.append(sum(history[-100:])/Decimal(len(history[-100:])))

    removect += 1
    if removect >= 1000:
        history.pop(0)
        xar.pop(0)
        yar.pop(0)
        removect = 0

    ax1.clear()
    ax1.plot_date(xar,history,"o", markersize=2)
    ax1.plot(xar, yar)


ani = animation.FuncAnimation(fig, animate)
plt.show()
